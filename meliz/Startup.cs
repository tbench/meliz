﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(meliz.Startup))]
namespace meliz
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
